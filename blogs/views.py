from datetime import datetime

from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404, render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson as json
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.forms.models import modelformset_factory

from blogs.models import FeedHit, Post, Section, Revision
from blogs.forms import AdminPostForm, ContactUsForm, SignUp, SignIn, AddNewPost
from blogs.settings import ALL_SECTION_NAME
from blogs.signals import post_viewed


@login_required(login_url = "login/")
def blog_index(request):
    user = request.user
    sections = Section.objects.all()
    posts = Post.objects.filter(author_id = user.id)
    
    return render_to_response("blogs/blog_list.html", {
        "posts": posts,
        "sections": sections,
    }, context_instance=RequestContext(request))

@login_required(login_url = "login/")
def blog_section_list(request, section_slug):
    sections = Section.objects.all()
    section = get_object_or_404(Section, slug=section_slug)
    user = request.user
    if section.id is not 4:
        posts = Post.objects.filter(author_id = user.id).filter(section = section.id)
    else:
        posts = Post.objects.filter(author_id = user.id)
    return render_to_response("blogs/blog_section_list.html", {
        "section_slug": section_slug,
        "section_name": section.name,
        "posts": posts,
        "sections": sections,
    }, context_instance=RequestContext(request))

@login_required(login_url = "login/")
def blog_post_detail(request, **kwargs):
    sections = Section.objects.all()
    if "post_pk" in kwargs:
        if request.user.is_authenticated() and request.user.is_staff:
            queryset = Post.objects.all()
            post = get_object_or_404(queryset, pk=kwargs["post_pk"])
        else:
            raise Http404()
    else:
        queryset = Post.objects.current()
        queryset = queryset.filter(
            published__year = int(kwargs["year"]),
            published__month = int(kwargs["month"]),
            published__day = int(kwargs["day"]),
        )
        post = get_object_or_404(queryset, slug=kwargs["slug"])
        post_viewed.send(sender=post, post=post, request=request)
    
    return render_to_response("blogs/blog_post.html", {
        "post": post,
        "sections": sections,
    }, context_instance=RequestContext(request))


def serialize_request(request):
    data = {
        "path": request.path,
        "META": {
            "QUERY_STRING": request.META.get("QUERY_STRING"),
            "REMOTE_ADDR": request.META.get("REMOTE_ADDR"),
        }
    }
    for key in request.META:
        if key.startswith("HTTP"):
            data["META"][key] = request.META[key]
    return json.dumps(data)


def blog_feed(request, section=None):
    sections = Section.objects.all()
    section = get_object_or_404(Section, slug=section)
    posts = Post.objects.filter(section=section)

    if section is None:
        section = Section.objects.values_list('name', flat=True).order_by('name')

    current_site = Site.objects.get_current()
    
    feed_title = "%s Blog: %s" % (current_site.name, section[0].upper() + section[1:])
    
    blog_url = "http://%s%s" % (current_site.domain, reverse("blog"))
    
    url_name, kwargs = "blog_feed", {"section": section}
    feed_url = "http://%s%s" % (current_site.domain, reverse(url_name, kwargs=kwargs))
    
    if posts:
        feed_updated = posts[0].published
    else:
        feed_updated = datetime(2009, 8, 1, 0, 0, 0)
    
    # create a feed hit
    hit = FeedHit()
    hit.request_data = serialize_request(request)
    hit.save()
    
    atom = render_to_string("blogs/atom_feed.xml", {
        "feed_id": feed_url,
        "feed_title": feed_title,
        "blog_url": blog_url,
        "feed_url": feed_url,
        "feed_updated": feed_updated,
        "entries": posts,
        "current_site": current_site,
        "sections": sections,
    })
    return HttpResponse(atom, mimetype="application/atom+xml")

def contact_us(request):
    sections = Section.objects.all()
    if request.method == "POST":
        form = ContactUsForm(request.POST)
        if form.is_valid():
            sender = form.cleaned_data["sender"]
            subject = form.cleaned_data["subject"]
            message = form.cleaned_data["message"]
            cc_myself = form.cleaned_data["cc_myself"]

            recepients = ["jvdagaang@live.com.ph"]

            if cc_myself:
                recepients.append(sender)

            send_mail(subject, message, sender, recepients)
        HttpResponseRedirect("/")
    else:
        form = ContactUsForm()

    return render(request, "blogs/blog_contact_us.html",{
                "form": form,
                "sections": sections,
        })

def sign_up(request):
    sections = Section.objects.all()
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            email = form.cleaned_data["email"]
            new_user = User.objects.create_user(username, email, password)
            new_user.first_name = form.cleaned_data["firstname"]
            new_user.last_name = form.cleaned_data["lastname"]
            new_user.save()

        return HttpResponseRedirect("/")
    else:
        form = SignUp()

    return render(request, "blogs/blog_sign_up.html",{"form": form,"sections":sections})

def sign_in(request):
    sections = Section.objects.all()
    if request.method == "POST":
        form = SignIn(request.POST)
        if form.is_valid():
            un = form.cleaned_data["username"]
            pw = form.cleaned_data["password"]

            user = authenticate(username = un, password = pw)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect("/")
                else:
                    return HttpResponseRedirect("sign_in/")
            else:
                return HttpResponseRedirect("sign_in/")
    else:
        form = SignIn()

    return render(request, "blogs/blog_sign_in.html", {"form": form, "sections": sections})

def sign_out(request):
    logout(request)
    return HttpResponseRedirect("/")

def add_post(request):
    sections = Section.objects.all()
    if request.method == "POST":
        form = AddNewPost(request.POST)
        if form.is_valid():
            post_slug = form.cleaned_data["title"]
            post_slug = post_slug.lower().replace(" ", "-")
            form.slug - post_slug

            form.author_id = request.user

            form.save()
            return HttpResponseRedirect("/")
        else:
            pass
    else:
        form = AddNewPost()

    return render(request, "blogs/blog_add_post.html", {"form": form, "sections": sections})

def manage_post(request):
    sections = Section.objects.all()
    user = request.user
    posts = Post.objects.filter(author_id = user.id)

    return render(request, "blogs/blog_manage_posts.html", {"sections": sections, "posts": posts})

def delete_post(request, post_pk):
    sections = Section.objects.all()
    post = Post.objects.get(id = post_pk)
    post.delete()

    return HttpResponseRedirect("/")

def update_post(request, post_pk):
    sections = Section.objects.all()
    post = Post.objects.get(id = post_pk)
    form = AdminPostForm(request.POST, instance = post)
    if request.method == "POST":
        if form.is_valid():
            form.save()

            return HttpResponseRedirect("/")
    else:
        form = AdminPostForm(instance = post)

    return render_to_response("blogs/blog_update_posts.html", {"sections": sections, "form": form}, context_instance = RequestContext(request))
