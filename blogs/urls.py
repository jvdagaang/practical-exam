from django.conf.urls import url, patterns


urlpatterns = patterns("",
    url(r'^contactUs/$', "blogs.views.contact_us", name="contact_us"),
    url(r'^register/$', "blogs.views.sign_up", name="register"),
    url(r'^login/$', "blogs.views.sign_in", name="login"),
    url(r'^logout/$', "blogs.views.sign_out", name="logout"),
    url(r'^add_post/$', "blogs.views.add_post", name="add_post"),
    url(r'^delete_post/(?P<post_pk>\d+)$', "blogs.views.delete_post", name="delete_post"),
    url(r'^manage_posts/$', "blogs.views.manage_post", name="manage_posts"),
    url(r'^update/(?P<post_pk>\d+)/$', "blogs.views.update_post", name="update_post"),
    url(r'^$', "blogs.views.blog_index", name="home"),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-\w]+)/$', "blogs.views.blog_post_detail", name="blog_post"),
    url(r'^post/(?P<post_pk>\d+)/$', "blogs.views.blog_post_detail", name="blog_post_pk"),
    url(r'^(?P<section_slug>[-\w]+)/$', "blogs.views.blog_section_list", name="blog_section"),
)